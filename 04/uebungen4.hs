findMinimum :: (Ord a) => [a] -> a
findMinimum (x:xs) = help' x xs
    where
        help' candidate [] = candidate
        help' candidate (x:xs) 
            | x < candidate = help' x xs
            | otherwise = help' candidate xs
