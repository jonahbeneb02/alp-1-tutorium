---
theme: "metropolis"
aspectratio: 169
---

# Tutorial 03

complexity analysis, sorting algorithm basics, strictness

# Statistics

\begin{align*}
&\text{Number of non-compilable submissions: } & 4/33 \\
&\text{Of which were in PDF: } & 1/33
\end{align*}

\begin{center}
\includegraphics[width=200px]{abgabememe.png}
\end{center}

# Strictness

## Strictness in functions

- A function is strict, when all its arguments are strict

## Strictness in arguments

- A function is strict in an argument when:
    - The argument is evaluated in all function definitions and branches
    - The argument is used for pattern matching

## Testing in ghci

- If a function can be called without errors with one of its arguments being `undefined`, it is not strict.
- Every Argument for which `undefined` can be used is not strict.

# Runtime complexity

```Haskell
(++) :: [a] -> [a] -> [a]
(++) []     ys = ys
(++) (x:xs) ys = x : xs ++ ys

mersenneZahlen' 0 = []
mersenneZahlen' n = mersenneZahlen' (n-1) ++ [2^n-1]
```
# Laufzeit

```Haskell
(++) :: [a] -> [a] -> [a]
(++) []     ys = ys           -- O(1)
(++) (x:xs) ys = x : xs ++ ys -- O(n) wo n = length of the first argument

mersenneZahlen' 0 = []
mersenneZahlen' n = 
    mersenneZahlen' (n-1) ++ [2^n-1]  -- O(n^2)
```
