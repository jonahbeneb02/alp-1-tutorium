---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 03

Komplexitätsanalyse, Sortieralgorithmen, Stiktheit

# Statistik

\begin{align*}
&\text{Anzahl nicht-kompilierender Abgaben: } & 4/33 \\
&\text{Davon Anzahl Abgaben von Code in PDF: } & 1/33
\end{align*}

\begin{center}
\includegraphics[width=200px]{abgabememe.png}
\end{center}

# Striktheit

## Striktheit bei Funktionen

- Eine Funktion ist strikt, wenn alle ihre Argumente strikt sind

## Striktheit in Argumenten

- Eine Funktion ist strikt in einem Argument wenn:
    - das Argument in allen Funktionsdefinitionen für die Rückgabe ausgewertet wird
    - oder das Argument für pattern matching verwendet wird

## Testen in ghci

- Wenn eine Funktion ohne Fehler mit einem ihrer Argumente als `undefined` aufgerufen werden kann, ist sie nicht strikt.
- Jedes Argument für das `undefined` eingesetzt werden kann ist nicht strikt.

# Laufzeit

```Haskell
(++) :: [a] -> [a] -> [a]
(++) []     ys = ys
(++) (x:xs) ys = x : xs ++ ys

mersenneZahlen' 0 = []
mersenneZahlen' n = mersenneZahlen' (n-1) ++ [2^n-1]
```
# Laufzeit

```Haskell
(++) :: [a] -> [a] -> [a]
(++) []     ys = ys           -- O(1)
(++) (x:xs) ys = x : xs ++ ys -- O(n) wo n = länge von erstem Argument

mersenneZahlen' 0 = []
mersenneZahlen' n = 
    mersenneZahlen' (n-1) ++ [2^n-1]  -- O(n^2)
```
