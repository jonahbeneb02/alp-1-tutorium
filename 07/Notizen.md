- Foliensatz

- Aufgabe 1: in Einzelarbeit oder mit Sitznachbar*in
- Aufgabe 2: in Gruppen, Aufgaben a und b parallel.
             Hinterher direkt vorrechnen / vergleichen
- Aufgabe 3 und 4 wieder mit Sitznachbar*in, so dass möglichst alle mindestens eine Aufgabe in der ein ADT erzeugt wird, und eine mit Pattern Matching bearbeitet haben.

- Vorrechnen
