---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 03

Auswertungsstrategien, Rekursion auf Listen

# Statistik

\begin{align*}
&\text{Anzahl nicht-kompilierender Abgaben: } & 9/33 \\
&\text{Davon Anzahl Abgaben von Code in PDF: } & 7/33
\end{align*}

\begin{center}
\includegraphics[width=200px]{pdf-abgaben.png}
\end{center}

# Aufgabe 1: Auswertungsstrategien

Bearbeitung in Gruppen

# Rekursion

**Beispiel**: Eine Funktion, die alle Namen die mit "A" beginnen aus einer Liste entfernt:
```Haskell
removeNamesA :: [String] -> [String]
removeNamesA [] = []
removeNamesA (x:xs)
    -- Fall 1: erstes Zeichen des Namens ist 'A'
    --         Name wird nicht wieder an die Ergebnisliste angefügt,
    --         und die restlichen Namen werden geprüft
    | head x == 'A'  = removeNamesA xs    
    -- Fall 2: Name beginnt nicht mit 'A'
    --         Name wird an die Ausgabeliste angefügt,
    --         und die restlichen Namen werden geprüft
    | otherwise      = x : removeNamesA xs
```

# Übung zu Rekursion

Aufgabe 2 und 3 **oder** Aufgabe 4 und 5 zu zweit bearbeiten

