removeNamesA :: [String] -> [String]
removeNamesA [] = []
removeNamesA (x:xs)
    -- Fall 1: erstes Zeichen des Namens ist 'A' 
    | head x == 'A'  = removeNamesA xs    
    -- Fall 2: Name beginnt nicht mit 'A'
    | otherwise      = x : removeNamesA xs

-- /////////////////////// Aufgabe 1 /////////////////////////
lisa x y z = stefan x (2*y) + anna y (3*y) (z**2) - ben (z+17)

stefan a _ = a

anna 0 b c = b+3
anna a b c = b + anna (a-1) b c

ben x = 30

{-
Call by value:

lisa (3**2) 2 4
lisa 9 2 4
stefan 9 (2*2) + anna 2 (3*2) (4**2) - ben (4 + 17)
stefan 9 4 + anna 2 6 16 - ben 21                     -- NOTE: Ich habe hier alle Argumente auf einmal ausgewertet, um
                                                      --       nicht zu viele Zeilen zu brauchen. Eigentlich wären das 
                                                      --       auch noch einzelne Schritte.
9          + 6 + anna (2-1) 6 16 - ben 21
9          + 6 + anna 1 6 9 - ben 21
9          + 6 + 6 + anna (1-1) 6 16 - ben 21
9          + 6 + 6 + anna 0 6 16 - ben 21
9          + 6 + 6 + 6 + 3 - 30
15 + 6 + 6 + 3 - 30
21 + 6 + 3 - 30
27 + 3 - 30
30 - 30
0

Call by name:

lisa (3**2) 2 4
stefan (3**2) (2*2) + anna 2 (3*2) (4 ** 2) - ben (4 + 17)
(3**2) + anna 2 (3*2) (4 ** 2) - ben (4 + 17)
(3**2) + (3*2) + anna 1 (3*2) (4 ** 2) - ben (4 + 17)
(3**2) + (3*2) + (3*2) + anna 0 (3*2) (4 ** 2) - ben (4 + 17)
(3**2) + (3*2) + (3*2) +  (3*2) + 3 - ben (4 + 17)
(3**2) + (3*2) + (3*2) +  (3*2) + 3 - 30
9 + 6 + (3*2) + (3*2) + 3 - 30
15 + (3*2) + (3*2) + 3 - 30
15 + 6 + (3*2) + 3 - 30
21 + (3*2) + 3 - 30
21 + 6 + 3 - 30
27 + 3 - 30
30 - 30
0

Lazy Evaluation:
lisa (3**2) 2 4
stefan (3**2) (2*2) + anna 2 (3*2) (4**2) - ben (4 + 17)
(3**2) + anna 2 (3*2) (4**2) - ben (4 + 17)
(3**2) + (3*2) + anna 1 (3*2) (4**2) - ben (4 + 17)
(3**2) + (3*2) + (3*2) + anna 0 (3*2) (4**2) - ben (4 + 17)
(3**2) + (3*2) + (3*2) + (3*2) + 3 - ben (4 + 17)
(3**2) + (3*2) + (3*2) + (3*2) + 3 - 30                       -- alle gleichen Ausdrücke werden auf einmal ersetzt
9 + 6 + 6 + 6 + 3 - 30
15 + 6 + 6 + 3 - 30
21 + 6 + 3 - 30
27 + 3 - 30
30 - 30
0
-}

-- /////////////////////// Aufgabe 2 /////////////////////////
sumOddNumbersSmallerThanN :: (Eq a, Integral a) => a -> [a] -> a
sumOddNumbersSmallerThanN _ [] = 0
sumOddNumbersSmallerThanN n (x:xs)
  | mod x 2 /= 0 && x < n = x + sumOddNumbersSmallerThanN n xs
  | otherwise = sumOddNumbersSmallerThanN n xs

-- /////////////////////// Aufgabe 3 /////////////////////////
dropOddNumbers :: (Eq a, Integral a) => [a] -> [a]
dropOddNumbers [] = []
dropOddNumbers (x:xs)
  | mod x 2 == 0 = x : dropOddNumbers xs
  | otherwise = dropOddNumbers xs

-- /////////////////////// Aufgabe 4 /////////////////////////
pairwiseSum :: (Num a) => [a] -> [a] -> [a]
pairwiseSum [] [] = []
pairwiseSum xs [] = xs
pairwiseSum [] ys = ys
pairwiseSum (x:xs) (y:ys) = (x + y) : pairwiseSum xs ys

-- /////////////////////// Aufgabe 5 /////////////////////////
dropWhileXGreaterThanThree :: (Ord a, Num a) => [a] -> [a]
dropWhileXGreaterThanThree [] = []
dropWhileXGreaterThanThree (x:xs)
  | x > 3 = dropWhileXGreaterThanThree xs
  | otherwise = x:xs

-- /////////////////////// Tests /////////////////////////
_1 = lisa (3**2) 2 4 == 0.0
_2 = sumOddNumbersSmallerThanN 5 [1, 3, 2, 2, 5, 9, 4, 3] == 7
_3 = dropOddNumbers [7, 14, 21, 5, 10, 2, 9] == [14,10,2]
_4 = pairwiseSum [4, 3, 7, 3] [5, -2, 3, 0] == [9,1,10,3]
_5 = dropWhileXGreaterThanThree [6, 4, 7, 4, 3, 9, 11, 1] == [3,9,11,1]
