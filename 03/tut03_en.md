---
theme: "metropolis"
aspectratio: 169
---

# Tutorial 03

Evaluation strategies, recursion on lists

# Statistics

\begin{align*}
&\text{Number of non-compilable submissions: } & 9/33 \\
&\text{Of which number of code submissions in PDF: } & 7/33
\end{align*}

\begin{center}
\includegraphics[width=200px]{pdf-abgaben.png}
\end{center}

# Task 1: Evaluation strategies

Work on task 1 in groups

# Recursion

**Example**: A function which removes all names from a list that start with the character 'A'
```Haskell
removeNamesA :: [String] -> [String]
removeNamesA [] = []
removeNamesA (x:xs)
    -- Case 1: first character of the name is 'A'
    --         Name is not added back to the result list,
    --         and the remaining names are checked
    | head x == 'A'  = removeNamesA xs    
    -- Case 2: Name does not start with 'A'
    --         Name is added to the output list,
    --         and the remaining names are checked
    | otherwise      = x : removeNamesA xs
```

# Recursion exercise

Work on task 2 and 3 **or** task 4 and 5 in pairs
