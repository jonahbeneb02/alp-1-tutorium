---
theme: metropolis
aspectratio: 169
---

# Tutorium 9

Bäume, Instanzen von Typklassen

# Instanzen von Typklassen

```Haskell
instance Show Datentyp where
    show x = ...
```
