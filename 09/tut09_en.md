---
theme: metropolis
aspectratio: 169
---

# Tutorial 9

Trees, instances of typeclasses

# Instances of Typeclasses

```Haskell
instance Show Datatype where
    show x = ...
```
