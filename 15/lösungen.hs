data Set = Union Set Set | Intersection Set Set | Complement Set | Set Char deriving Show


deMorgan (Complement (Union a b))        = Intersection (Complement a) (Complement b)
deMorgan (Complement (Intersection a b)) = Union (Complement a) (Complement b)

deMorgan (Complement x) = Complement (deMorgan x)
deMorgan (Intersection x y) = Intersection (deMorgan x) (deMorgan y)
deMorgan (Union x y) = Union (deMorgan x) (deMorgan y)
deMorgan x = x
