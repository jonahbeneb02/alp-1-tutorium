module Calc(upn) where

import Stack

-- calc :: (Num a, Read a) => [ String ] -> Stack a
-- calc = foldl handle emptyStack
--     where
--         handle s "+" = applyToTop2 (+) s
--         handle s "-" = applyToTop2  (-) s
--         handle s "*" = applyToTop2 (*) s
--         handle s str = push ( read str ) s
--         applyToTop2 f s
--             | size s < 2 = error " not enough values "
--             | otherwise = push ( x2 `f ` x1 ) s2
--             where
--                 Just (x1 , s1) = pop s
--                 Just (x2 , s2) = pop s1
--
-- valid :: Stack a -> a
-- valid s
--     | size s == 1 = result
--     | isEmpty s = error " empty input "
--     | otherwise = error " too many values left "
--     where
--         (Just result) = peek s
--
-- upn :: (Num a, Read a) => String -> a
-- upn = valid . calc . words
--

-- calc :: ( Num a , Read a, Integral a) => [ String ] -> Stack a
-- calc = foldl handle emptyStack
--     where
--         handle s "+" = applyToTop2 (+) s
--         handle s "-" = applyToTop2 (-) s
--         handle s "*" = applyToTop2 (*) s
--         handle s "/" = applyToTop2 (div) s
--         handle s str = push ( read str ) s
--         applyToTop2 f s
--             | size s < 2 = error " not enough values "
--             | otherwise = push ( x2 `f ` x1 ) s2
--             where
--                 Just (x1 , s1) = pop s
--                 Just (x2 , s2) = pop s1
-- 
-- valid :: Stack a -> a
-- valid s
--     | size s == 1 = result
--     | isEmpty s = error " empty input "
--     | otherwise = error " too many values left "
--     where
--         (Just result) = peek s
-- 
-- upn :: ( Num a , Read a, Integral a) => String -> a
-- upn = valid . calc . words


calc' :: ( Num a , Read a, Floating a) => [ String ] -> Stack a
calc' = foldl handle emptyStack
    where
        handle s "+" = applyToTop2 (+) s
        handle s "-" = applyToTop2  (-) s
        handle s "*" = applyToTop2 (*) s
        handle s "log" = applyToTop1 (logBase 10) s
        handle s "/" = applyToTop2 (/) s
        handle s str = push ( read str ) s
        applyToTop1 :: (a -> a) -> Stack a -> Stack a
        applyToTop1 f s
            | size s < 1 = error "no values"
            | otherwise = push (f x1) s1
            where
                Just (x1, s1) = pop s
        applyToTop2 f s
            | size s < 2 = error " not enough values "
            | otherwise = push ( x2 `f ` x1 ) s2
            where
                Just ( x1 , s1 ) = pop s
                Just ( x2 , s2 ) = pop s1

valid' :: Stack a -> a
valid' s
    | size s == 1 = result
    | isEmpty s = error " empty input "
    | otherwise = error " too many values left "
    where
        ( Just result ) = peek s

upn :: ( Num a , Read a, Floating a ) => String -> a
upn = valid' . calc' . words
