{--
Funktionale Programmierung - Wintersemester 2022/2023
Vorlesung am 04.01.2023
Katharina Klost

Modul für Stacks
--}

module Stack(Stack, emptyStack, size, push, pop, peek, isEmpty) where
  -- Stack Einträge speichern zusätzlich die Größe des Stacks
  data Stack a = Empty | S Int a (Stack a) deriving Eq


  instance (Show a) => Show (Stack a) where
    show s = '<' : reverse (go s [])
       where
         go:: (Show a) => Stack a -> String -> String
         go Empty acc = ']': acc
         go (S _ x Empty) acc = ']': ((reverse.show) x) ++ acc
         go (S _ x stack) acc = go stack $ " ," ++ ((reverse.show) x) ++ acc

  {-
  Voraussetzung: Keine
  Ergebnis: Ein leerer Stack ist geliefert
  -}
  emptyStack :: Stack a
  emptyStack = Empty

  {-
  Voraussetzung: Keine
  Ergebnis: Die Anzahl der Elemente auf dem Stack ist geliefert
  -}
  size :: Stack a -> Int
  size Empty = 0
  size (S size _ _) = size

  {-
  Voraussetzung: Keine
  Ergebnis: Ein Stack mit x als oberstem Element ist geliefert
  -}
  push:: a ->  Stack a -> Stack a
  push x Empty = S 1 x Empty
  push x stack@(S size _ _ ) = S (size + 1) x stack

  {-
  Voraussetzung: Keine
  Ergebnis: Wenn der Stack leer ist, ist Nothing geliefert, sonst Just ein Tupel aus dem obersten Element
  und dem restlichen Stack
  -}
  pop:: Stack a -> Maybe (a, Stack a)
  pop Empty = Nothing
  pop (S _ x s) = Just (x,s)

  {-
  Voraussetzung: Keine
  Ergebnis: Wenn der Stack leer ist, ist Nothing geliefert, sonst Just das oberste Element
  -}
  peek :: Stack a -> Maybe a
  peek Empty = Nothing
  peek (S size x _) = Just x

  {-
  Voraussetzung: Keine
  Ergebnis: Wenn der Stack leer ist, ist True geliefert, sonst ist False geliefert
  -}
  isEmpty :: Stack a -> Bool
  isEmpty Empty = True
  isEmpty _     = False
