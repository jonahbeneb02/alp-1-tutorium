---
theme: metropolis
aspectratio: 169
header-includes: |
    \usepackage{qrcode}
    \include{hyperref}
    \include{amsmath}
---


# 

```Haskell
add3 a b c = a + b + c
```
\
**Evaluated**:
```Haskell
add3 1 2 3
(\a -> \b -> \c -> a + b + c) 1 2 3
(\b -> \c -> 1 + b + c) 2 3
(\c -> 1 + 2 + c) 3
1 + 2 + 3
```

# Lambda-Calculus

less formal, hopefully easier to understand summary

# Variables

* Variable names are just one character

**Exanple**:

* $(\lambda xyz.xyz)$ has three parameters

# Variable binding

\begin{center}
\huge{($\lambda$ $V.E$)}
\end{center}

* A variable is bound, if it appears in V
* E can be another lambda expression. Variables of the same name are also bound in there.
* In a expression consisting of multiple lambda expressions, a variable is bound if it is bound in one of the expressions

**Examples**:

* $(\lambda x.x)$ - x is bound
* $(\lambda x.y)$ - y is not bound
* $(\lambda x.(\lambda y.x))$ - x is bound


# $\beta$-reduction (function application)

\begin{center}
\huge{($\lambda$ $x.E$) y}
\end{center}

* The expression in parantheses has one parameter, $x$
* When evaluating it with the given value $y$, the value of $y$ is inserted for $x$.

**Example**:

* $(\lambda x.x)y \equiv_{\beta} y$

# $\alpha$-conversion (renaming)

\begin{center}
\huge{($\lambda$ $V.E$) x}
\end{center}

If $x$ would be bound in $E$, $x$ needs to be renamed in $V$ and $E$ before applying the expression to $x$

**Example**:

* $(\lambda xy.x) y \equiv_{\alpha} (\lambda xt.x) y$
