{-

T_merge(n) = O(1)
T_merge(n) = O(1)
T_merge(n) = O(m)       m = length (x:xs) + (y:ys)

T_zipWidth(n) = O(o)    o = min (length xs, length ys)

T_mergeLists  = O(n)    n = m * o

-}

cartesianProduct :: [(a -> b)] -> [a] -> [b]
cartesianProduct [] xs = []
cartesianProduct (f:funcs) xs = map f xs ++ cartesianProduct funcs xs

removeBrackets :: String -> String
removeBrackets [] = []
removeBrackets (x:xs)
    | isBracket x = removeBrackets xs
    | otherwise = x : removeBrackets xs
    where
        isBracket c = c == '(' || c == ')' || c == '{' || c == '}' || c == '[' || c == ']' || c == '<' || c == '>'

checkTuples :: [(a, b)] -> (a -> Bool) -> [b]
checkTuples xs f = map snd (filter (\x -> f (fst x)) xs)

checkTuples' :: [(a, b)] -> (a -> Bool) -> [b]
checkTuples' xs f = map snd (filter (f . fst) xs)

flipCapitalization :: String -> String
flipCapitalization = map flipChar
    where
        flipChar c
            | isUpper c = toLower c
            | otherwise = toUpper c

        isUpper c = c >= 'A' && c <= 'Z'
        isLower c = c >= 'a' && c <= 'z'

        upperOffset = fromEnum 'a' - fromEnum 'A'
        toUpper c
            | isLower c = toEnum (fromEnum c - upperOffset)
            | otherwise = c
        toLower c
            | isUpper c = toEnum (fromEnum c + upperOffset)
            | otherwise = c
