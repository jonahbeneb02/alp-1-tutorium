---
theme: "metropolis"
aspectratio: 169
---

## Tutorial 6

Currying, type inference, tail recursion, folds

## (very) small lambda calculus intro

* A lambda-expression has just one parameter
* It can "remeber" variables from its environment
* lambda-expressions can be written using anonymous functions in haskell

## Example

```Haskell
divisibleBy :: Int -> [Int] -> [Int]
divisibleBy divisor numbers =
    filter 
        (\a -> a `mod` divisor == 0)
        numbers
```

* The anonymous functions that is passed to `filter` only has one parameter
* The variable `divisor` is inserted into the expression before it is passed on to filter.

## Currying

Any number of function parameters can be written by combining functions which only have one parameter.

## Examples

This function adds three numbers

```Haskell
add3 a b c = a + b + c
```

Function parameters can be gradually inserted into the final expression by using anonymous functions:

```Haskell
add3 = \a -> \b -> \c -> a + b + c
```

## Evaluation example

```Haskell
add3 1 2 3
(\a -> \b -> \c -> a + b + c) 1 2 3
-- The first function returns the second one after inserting its parameter
(\b -> \c -> 1 + b + c) 2 3
-- The second function returns the third function
(\c -> 1 + 2 + c) 3
-- Evaluating the final expression yields the result
1 + 2 + 3
```

## Usage

Currying allows inserting values into existing functions

```Haskell
add2 :: Int -> Int -> Int
add2 = add3 2
```

## Usage

```Haskell
add2 :: Int -> Int -> Int
add2 = add3 2
add2 = (\a -> \b -> \c -> a + b + c) 2
add2 = (\b -> \c -> 2 + b + c)
```

The function signature is correct, only 2 parameters remain.


## Type inference examples

```Haskell
f = (+)
```

## Type inference examples

```Haskell
f :: (Num a) -> a -> a -> a
f = (+)
```


## Type inference examples

```Haskell
g = (+) 2
```

## Type inference examples

```Haskell
g :: (Num a) -> a -> a
g = (+) 2
```

## Type inference examples

```Haskell
h = map (>10) . filter (\x -> x `mod` 2 == 0)
```

## Type inference examples

```Haskell
h :: Integral a => [a] -> [Bool]
h = map (>10) . filter (\x -> x `mod` 2 == 0)
```

## Folds

Example: Tail-recursive sum function

```Haskell
sum :: Num a => [a] -> a
sum xs = aux xs 0
    where
        aux (x:xs) acc = aux xs (x + acc)
```

Equivalent using `foldr`
```Haskell
sum :: Num a => [a] -> a
sum xs = foldr (\x acc -> x + acc) 0 xs
```
