-- f :: (Eq a, Eq c, Num c) => [a] -> [a] -> a -> [a]
-- 
-- g :: [a] -> [[a]]
-- 
-- h :: [[a]] -> Int
-- 


divisors :: Int -> [Int]
divisors 0 = []
divisors n = aux n n
    where
        aux n 0 = []
        aux n d
            | n `mod` d == 0 = d : aux n (d - 1)
            | otherwise      = aux n (d - 1)

divisors' :: Int -> [Int]
divisors' 0 = []
divisors' n = aux n n []
    where
        aux n 0 acc = acc
        aux n d acc
            | n `mod` d == 0 = aux n (d - 1) (d : acc)
            | otherwise      = aux n (d - 1) acc

divisors'' :: Int -> [Int]
divisors'' 0 = []
divisors'' n = aux n n []
    where
        aux n 0 acc = acc
        aux n d acc
            | n `mod` d == 0 = aux n (d - 1) ((:) d $! acc)
            | otherwise      = aux n (d - 1) acc

divisors''' :: Int -> [Int]
divisors''' n = foldr (\d acc -> if n `mod` d == 0 then d : acc else acc) [] [1..n]

lengthFolded :: [a] -> Int
lengthFolded = foldr (\_ acc -> 1 + acc) 0

counts :: (Eq a) => a -> [a] -> Int
counts x = foldr (\e acc -> if x == e then 1 + acc else acc) 0

mostly :: (a -> Bool) -> [a] -> Bool
mostly cond xs = counts True (map cond xs) >= length xs `div` 2
