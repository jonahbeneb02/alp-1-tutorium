module Queue(Queue(F), enqueue, dequeue, isEmpty, hasElem, count) where

class Queuelike s where
  {-
  Voraussetzung: Keine
  Ergebnis: Eine neue Queue in die das Element hinten eingefügt ist, ist geliefert
  -}
  enqueue :: a -> s a -> s a

  {-
  Voraussetzung: Keine
  Ergebnis: Wenn die Queue leer ist, ist Nothing geliefert, sonst Just ein Tupel
            aus dem vordesten Element und der Queue ohne das vorderste Element
  -}
  dequeue :: s a -> Maybe (a, s a)

  {-
  Voraussetzung: Keine
  Ergebnis: Wenn die Queue leer ist, ist False geliefert, sonst ist True geliefert
  -}
  isEmpty :: s a -> Bool

  hasElem :: (Eq a) => a -> s a -> Bool
  count :: (Eq a) => a -> s a -> Int


data Queue a = F [a] [a] deriving Show

instance Queuelike Queue where
    enqueue x (F inp out ) = (F (x:inp) out)

    dequeue (F [] []) = Nothing
    dequeue (F inp []) = dequeue (F [] (reverse inp))
    dequeue (F inp (x:out)) = Just (x, (F inp out))

    isEmpty (F [] []) = True
    isEmpty _         = False

    hasElem :: (Eq a) => a -> Queue a -> Bool
    hasElem e (F inp out) = elem e inp || elem e out

    count ::  (Eq a) => a -> Queue a -> Int
    count e (F inp out) = count' inp + count' out
        where
            count' xs = foldr (\x acc -> if x == e then acc + 1 else acc) 0 xs
