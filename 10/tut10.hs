import Queue
import Data.Maybe

data Produkt = Produkt String Int deriving Show
type Einkaufswagen = [Produkt]
type Schlange = Queue Einkaufswagen

anstellen :: Schlange -> Einkaufswagen -> Schlange
anstellen schlange wagen = enqueue wagen schlange

kassieren :: Schlange -> (Int, Schlange)
kassieren schlange = (preis, neueSchlange)
    where
        leereSchlange = F [] []
        leererEinkaufswagen = []

        (produkte, neueSchlange) = fromMaybe (leererEinkaufswagen, leereSchlange) $ dequeue schlange

        preis = foldr (\(Produkt _ preis) acc -> acc + preis) 0 produkte
