---
theme: "metropolis"
aspectratio: 169
header-includes: |
    \usepackage{amsmath}
---

# Tutorium 2

Typklassen, Pattern Matching, Listengeneratoren

# Pattern Matching

```Haskell
type Pair a = (a, a)
```
\
\
**Beispiel**: Funktion die das erste Element aus einem Paar zurückgibt:
```Haskell
fst :: Pair a -> a
fst pair = ...
```

# Pattern Matching

```Haskell
type Pair a = (a, a)
```
\
\
**Beispiel**: Funktion die das erste Element aus einem Paar zurückgibt:
```Haskell
fst :: Pair a -> a
fst (x, y) = x
```

# Typsignatur finden

Beispiel:
```Haskell
type Pair a = (a, a)

comparePairs (a, b) (x, y) = a == x && b == y
```

1. Grobe Typsignatur einfügen, platzhalter für Datentypen verwenden (kleine Buchstaben)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Sammeln, welche Operationen Datentypen unterstützen müssen
1. Typklassen, die die Operationen enthalten finden

# Typsignatur finden

```Haskell
comparePairs (a, b) (x, y) = a == x && b == y
```

1. Grobe Typsignatur einfügen, Platzhalter für Datentypen verwenden (kleine Buchstaben)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Sammeln, welche Operationen Datentypen unterstützen müssen
    * `x == y`
1. Typklassen, die die Operationen enthalten finden

# Typsignatur finden

```Haskell
comparePairs (a, b) (x, y) = a == x && b == y
```

1. Grobe Typsignatur einfügen, Platzhalter für Datentypen verwenden (kleine Buchstaben)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Sammeln, welche Operationen Datentypen unterstützen müssen
    * `x == y`
1. Typklassen, die die Operationen enthalten finden
    * Eq

Ergebnis:
```Haskell
comparePairs :: (Eq a) => Pair a -> Pair a -> Bool
```

# Listengeneratoren Wiederholung

Kartesisches Produkt:
$\{ (a, b) \text{ | } a \in A, b \in B \}$

Beispiel:
$\{a, b, c\} \times \{x, y, z\} = \{(a, x), (a, y), (a, z), (b, x), (b, y), (b, z), (c, x), ...\}$
\
\
\
Mit Listengenerator:
```Haskell
cartesianProduct :: [a] -> [b] -> [(a, b)]
cartesianProduct menge1 menge2 = [ ... | ... ]
```

# Listengeneratoren Wiederholung

```Haskell
cartesianProduct :: [a] -> [b] -> [(a, b)]
cartesianProduct menge1 menge2 = [ (x, y) | x <- menge1, y <- menge2 ]
```
