charIsNumber :: Char -> Bool
charIsNumber c = c >= '0' && c <= '9'

charIsNumber' '0' = True
charIsNumber' '1' = True
charIsNumber' '2' = True
charIsNumber' '3' = True
charIsNumber' '4' = True
charIsNumber' '5' = True
charIsNumber' '6' = True
charIsNumber' '7' = True
charIsNumber' '8' = True
charIsNumber' '9' = True
charIsNumber' _   = False

charIsNumber'' c
    | c >= '0' && c <= '9' = True
    | otherwise = False

charIsNumber''' c
    | c >= '0' = charIsNumberEnd c
    | otherwise = False
    where
        charIsNumberEnd c
            | c <= '9' = True
            | otherwise = False

charIsNumber'''' c = case c of
                          _ | c >= '0' -> case c of
                                               _ | c <= '9' -> True
                                                 | otherwise -> False
                            | otherwise -> False

charIsNumber''''' c
    | c < '0' = False
    | c <= '9' = True
    | otherwise = False

istSchaltjahr jahr = jahr `mod` 4 == 0 && (not (jahr `mod` 100 == 0) || jahr `mod` 400 == 0)

piratenschatzBedingung wert = wert `mod` 13 == 3 && wert `mod` 12 == 5 && wert `mod` 11 == 0
piratenschatz = [ wert | wert <- [0..], piratenschatzBedingung wert]
piratenschatzLimited = length [ wert | wert <- [0..1000], piratenschatzBedingung wert]
piratenschatzLimited' = length (takeWhile (<= 1000) piratenschatz)

mersenneZahlen n = [ 2^x-1 | x <- [1..n]]
mersenneZahlen' 0 = []
mersenneZahlen' n = mersenneZahlen' (n-1) ++ [2^n-1]

mersenneSmaller x = takeWhile (< x) (mersenneZahlen x)
