---
theme: "metropolis"
aspectratio: 169
header-includes: |
    \usepackage{amsmath}
---

# Tutorial 2

Type classes, pattern matching and list comprehensions

# Pattern Matching

```Haskell
type Pair a = (a, a)
```
\
\
**Example**: Function that returns the first element of a pair:
```Haskell
fst :: Pair a -> a
fst pair = ...
```

# Pattern Matching

```Haskell
type Pair a = (a, a)
```
\
\
**Example**: Function that returns the first element of a pair:
```Haskell
fst :: Pair a -> a
fst (x, y) = x
```

# Finding the type signature

Example:
```Haskell
type Pair a = (a, a)

comparePairs (a, b) (x, y) = a == x && b == y
```

1. Add the basic type signature, with placeholders for data types (in lowercase)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Collect the operations that are used on the values of type `a`
1. Find the type classes that provide these operations

# Finding the type signature

Example:
```Haskell
type Pair a = (a, a)

comparePairs (a, b) (x, y) = a == x && b == y
```

1. Add the basic type signature, with placeholders for data types (in lowercase)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Collect the operations that are used on the values of type `a`
    * `x == y`
1. Find the type classes that provide these operations

# Finding the type signature

1. Add the basic type signature, with placeholders for data types (in lowercase)
   ```Haskell
   comparePairs :: Pair a -> Pair a -> Bool
   ```
1. Collect the operations that are used on the values of type `a`
    * `x == y`
1. Find the type classes that provide these operations
    * Eq

Result:
```Haskell
comparePairs :: (Eq a) => Pair a -> Pair a -> Bool
```

# List comprehensions

Cartesian product:
$\{ (a, b) \text{ | } a \in A, b \in B \}$

Example:
$\{a, b, c\} \times \{x, y, z\} = \{(a, x), (a, y), (a, z), (b, x), (b, y), (b, z), (c, x), ...\}$
\
\
\
As a list comprehension:
```Haskell
cartesianProduct :: [a] -> [b] -> [(a, b)]
cartesianProduct menge1 menge2 = [ ... | ... ]
```

# List comprehensions

```Haskell
cartesianProduct :: [a] -> [b] -> [(a, b)]
cartesianProduct menge1 menge2 = [ (x, y) | x <- menge1, y <- menge2 ]
```
