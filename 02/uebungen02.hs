type Vector a = (a, a, a)

addVectors :: (Num a) => Vector a -> Vector a -> Vector a
addVectors (a1, a2, a3) (b1, b2, b3) = (a1 + b1, a2 + b2, a3 + b3)










scalarProduct :: (Num a) => Vector a -> Vector a -> a
scalarProduct (a1, a2, a3) (b1, b2, b3) = a1 * b1 + a2 * b2 + a3 * b3










isRightAngled :: (Num a, Eq a) => Vector a -> Vector a -> Bool
isRightAngled a b = scalarProduct a b == 0










pairwiseRightAngled :: (Num a, Eq a) => [Vector a] -> [Vector a] -> [(Vector a, Vector a)]
pairwiseRightAngled vectorsA vectorsB = [(x, y) | x <- vectorsA, y <- vectorsB, isRightAngled x y]













type Pair a = (a, a)

comparePairs :: (Eq a) => Pair a -> Pair a -> Bool
comparePairs (a, b) (x, y) = a == x && b == y
