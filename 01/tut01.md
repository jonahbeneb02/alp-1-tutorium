---
theme: "metropolis"
---

# Tutorium 1

organisatorische Fragen, erste Übungen

# Kriterien für die aktive Teilnahme

* 2x Aufgaben präsentieren
<!-- * 1x ein Kahoot-Quiz vorbereiten -->
* Anwesenheit in den Tutorien ist pflicht
* Übungsblätter
    * 60% der Punkte müssen insgesamt erreicht werden
    * es müssen n-2 Blätter bearbeitet werden
    * mindestens 20% müssen pro Übungszettel bearbeitet werden
* 30% müssen in Testaten erreicht werden

* 2x Abwesenheit in den Tutorien ist in Ordnung, für mehr ist eine Erklärung erforderlich

# Übungsblätter

* Übungsblätter werden in 2er-Gruppen abgegeben.
    * Pro Gruppe lädt eine Person das Ergebnis im Whiteboard hoch
    * Bitte die Namen aller Gruppenmitglieder auf die Abgabe schreiben.
* **Der Quellcode muss kompilierbar sein.**
* Eure Lösungen können anonymisiert veröffentlicht werden, wenn ihr zustimmt.
* Alle verwendeten Quellen müssen angegeben werden.


# Exam Booklet

* Muss mit der Hand geschrieben werden
* Schreiben auf Zeichentablets / Tablets ist in Ordnung


# Aufgaben präsentieren

- Jeder muss zweimal eine Aufgabe präsentieren.


# Kontakt

**Jonah Brüchert**

[jonahbeneb02@fu-berlin.de](mailto:jonahbeneb02@fu-berlin.de)


# Tutoriumsmaterial

![](material.png)

[git.imp.fu-berlin.de/jonahbeneb02/alp-1-tutorium](https://git.imp.fu-berlin.de/jonahbeneb02/alp-1-tutorium)


# Haskell-Compiler installieren

1. Testen ob ghci schon installiert ist:
    1. Terminal öffnen
    2. `ghci` starten
2. Wenn ghci noch nicht installiert ist, Anleitung unter [haskell.org/ghcup](https://www.haskell.org/ghcup/) folgen
    * haskell-language-server (HLS) auswählen
