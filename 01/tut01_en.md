---
theme: "metropolis"
---

# Tutorial 1

organisational things, installing Haskell

# Criteria for active participation

* presenting a task 2 times
* participation in the tutorials
* work sheets:
    * reaching 60% of the maximum points
    * handing in no less than n-2 
    * reaching at least 20% on every sheet
* tests:
    * reaching at least 30%
    
* You can be absent two times without providing a reason, afterwards a reason is required

# Work sheets

* Working in groups of two
    * One person per group hands in the work sheet digitally in the whiteboard.
    * Please write down the name of all group members on every submission.
* If the solution contains source code, it **needs to compile**
* Your solutions can be published anonymously for helping others, if you agree
* All sources and tools used need to be specified

# Exam Booklet

* Your own collection of the most important parts of each topics
* need to be written by hand
* Writing on graphic tablets or tablets is fine
* Can be used during the exam

# Presenting tasks

* Everyone needs to present a task at least twice in front of the tutorial

# Contact

**Jonah Brüchert**

[jonahbeneb02@fu-berlin.de](mailto:jonahbeneb02@fu-berlin.de)

# Tutorial slides

![](material.png)

[git.imp.fu-berlin.de/jonahbeneb02/alp-1-tutorium](https://git.imp.fu-berlin.de/jonahbeneb02/alp-1-tutorium)

# Installing a Haskell-Compiler

1. Test whether ghci is already installed on your system:
    1. open a terminal
    2. enter `ghci` and press enter
2. If ghci is not already installed, follow the steps at [haskell.org/ghcup](https://www.haskell.org/ghcup/)
    * Select the haskell-language-server (HLS) if asked for it by the installer script
