data BTree a = Nil | Node a ( BTree a ) ( BTree a ) deriving Show

postOrder :: BTree a -> [a]
postOrder Nil = []
postOrder (Node x lt rt) = postOrder lt ++ postOrder rt ++ [x]


postOrder' :: BTree a -> [a]
postOrder' tree = reverse (collect tree [])
    where
        collect Nil acc = acc
        collect (Node x lt rt) acc = x : (collect rt (collect lt acc))
