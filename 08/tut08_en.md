---
theme: metropolis
aspectratio: 169
---

# Tutorial 8

structural induction, algebraic datatypes, binary trees

# Example for Structural Induction
```Haskell
sum :: (Num a) => [a] -> a
sum [] = 0                              -- sum.1
sum (x:xs) = x + sum xs                 -- sum.2
```

**to show:**

`sum (xs ++ ys) = sum xs + sum ys`

# Example for Structural Induction

**Proposition:** `sum (xs ++ ys) = sum xs + sum ys`
**Base case:** 

```Haskell
xs = []

sum ([] ++ ys)
    = sum ys
    = 0 + sum ys        -- sum.1
    = sum [] + sum ys
```

# Example for Structural Induction

**Induction step:**
```Haskell
xs = (x:xs)

sum ((x:xs) ++ ys)         -- sum.2
    = x + sum (xs ++ ys)   -- proposition
    = x + sum xs + sum ys  -- sum.2
    = sum (x:xs) + sum ys
```

# Examples for Algebraic Data Types

```Haskell
data Number = I Int | F Float deriving Show
```
\
`I` and `F` are data constructors. They can be used like functions.

\
Creating a value of an ADT:
```Haskell
divide :: Int -> Int -> Number
divide n d
    | n `mod` d == 0 = I (n `div ` d)
    | otherwise      = F (fromIntegral n / fromIntegral d)
```

# Examples for Algebraic Data Types

```Haskell
data Number = I Int | F Float deriving Show
```
\
\
Handling different variants:
```Haskell
numberTypeString (I _) = "integer"
numberTypeString (F _) = "float"
```
