---
theme: metropolis
aspectratio: 169
---

# Tutorium 8

Strukturelle Induktion, Algebraische Datentypen

# Beispiel für Strukturelle Induktion
```Haskell
sum :: (Num a) => [a] -> a
sum [] = 0                              -- sum.1
sum (x:xs) = x + sum xs                 -- sum.2
```

**zu zeigen:**

`sum (xs ++ ys) = sum xs + sum ys`

# Beispiel für Strukturelle Induktion

**Induktionsvoraussetzung:** `sum (xs ++ ys) = sum xs + sum ys`
**Induktionsanfang:** 
zeigen für Startelement

```Haskell
xs = []

sum ([] ++ ys)
    = sum ys
    = 0 + sum ys        -- sum.1
    = sum [] + sum ys
```

# Beispiel für Strukturelle Induktion

**Induktionsschritt:**
```Haskell
xs = (x:xs)

sum ((x:xs) ++ ys)         -- sum.2
    = x + sum (xs ++ ys)   -- IV.
    = x + sum xs + sum ys  -- sum.2
    = sum (x:xs) + sum ys
```

# Beispiele für ADTs

```Haskell
data Number = I Int | F Float deriving Show
```
\
`I` und `F` sind Datenkonstruktoren. Sie können wie Funktionen verwendet werden.

\
ADTs erzeugen:
```Haskell
divide :: Int -> Int -> Number
divide n d
    | n `mod` d == 0 = I (n `div ` d)
    | otherwise      = F (fromIntegral n / fromIntegral d)
```
# Beispiele für ADTs

```Haskell
data Number = I Int | F Float deriving Show
```
\
\
Unterschiedliche Varianten eines ADTs behandeln:
```Haskell
numberTypeString (I _) = "integer"
numberTypeString (F _) = "float"
```
